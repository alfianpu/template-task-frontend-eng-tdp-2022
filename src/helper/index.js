export const formatNumberToRupiah = (number) => {
    return new Intl.NumberFormat('id-Id', { minimumFractionDigits: 0, maximumFractionDigits: 0 }).format(number) 
}