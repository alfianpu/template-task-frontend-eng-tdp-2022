import axios from "axios";

const fetcher = (url) => axios.get(url).then(data=>data.data.data)

export default fetcher