import { Link, useSearchParams } from "react-router-dom"
import fetcher from 'config/Fetcher';
import Breadcrumbs from "components/Breadcrumbs";
import useSWR from 'swr';
import Loading from "components/Loading";
import { formatNumberToRupiah } from "helper";
const Shop = () => {
    let [searchParams] = useSearchParams();
    const optionsSwr = {
        refreshInterval:0,
        revalidateOnFocus: false,
        shouldRetryOnError: false 
    }
    const { data, error } = useSWR(`https://api-tdp-2022.vercel.app/api/products${searchParams.get('category') ? `?category=${searchParams.get('category')}` : ''}`, fetcher,optionsSwr)
    const dataBreadcrumb = [
        { to: '/', label: 'Home' },
        { to: '', label: 'Shop' }
    ]
    return(
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container" style={{ marginTop: '80px' }}>
                <Breadcrumbs
                    data={dataBreadcrumb}
                />
            </div>
            <div className="container padding-32" id="about">
                <h3 className="border-bottom border-light-grey padding-16">
                    {searchParams.get('category') ? `Product Category ${data?.categoryDetail?.name ?? ''}` : 'All Product'}
                </h3>
            </div>
            {
                !data && !error && <Loading/>
            }
            {
                data?.productList && !error && <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                    {
                        data.productList.map((val,index)=>{
                            let price = formatNumberToRupiah(val.price)
                            return (
                                <div key={index} className="product-card">
                                    { val.discount ? <div className="badge">Discount</div> : ''}
                                    <div className="product-tumb">
                                        <img src={val.image} alt={val.title} />
                                    </div>
                                    <div className="product-details">
                                        <span className="product-catagory">{val.category}</span>
                                        <h4>
                                            <Link to={`/shop/${val.id}`}>{val.title}</Link>
                                        </h4>
                                        <p>{val.description}</p>
                                        <div className="product-bottom-details">
                                            <div className="product-price">Rp. {price}</div>
                                            <div className="product-links">
                                            <Link to={`/shop/${val.id}`}>View Detail</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                    
                    
                </div>
            }
            
            
        </div>
    )
}

export default Shop