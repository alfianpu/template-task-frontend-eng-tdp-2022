import { useParams, useNavigate } from 'react-router-dom'
import Breadcrumbs from "components/Breadcrumbs"
import Loading from 'components/Loading';
import fetcher from 'config/Fetcher';
import useSWR from 'swr';
import { formatNumberToRupiah } from 'helper';
import { useState } from 'react'
const Detail = () => {
    const [inputQty, setInputQty] = useState(0)
    let { id } = useParams();
    const dataBreadcrumb = [
        { to: '/', label: 'Home' },
        { to: '/shop', label: 'Shop' },
        { to: '', label: 'Detail' }
    ]
    const navigate = useNavigate()
    const optionsSwr = {
        refreshInterval:0,
        revalidateOnFocus: false,
        shouldRetryOnError: false 
    }
    const { data, error } = useSWR(id && `https://api-tdp-2022.vercel.app/api/products/${id}`, fetcher,optionsSwr)
    const handleInputQty = e => {
        const { value } = e.target
        if(value <= data.stock && value >= 0){
            setInputQty(value)
        }
    }
    const addToCart = () => {
        alert('Product added to cart successfully')
        navigate('/cart',{replace:true, state:{
            cart:'asdasdas'
        }})
    }
    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
        <div className="container" style={{ marginTop: '80px' }}>
            <Breadcrumbs 
                data={dataBreadcrumb}
            />
        </div>
        <div className="container padding-32" id="about">
            <h3 className="border-bottom border-light-grey padding-16">Product Information</h3>
        </div>
        {
            !data && !error && <Loading/>
        }
        {
            data && !error && <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
            <div className="col l3 m6 margin-bottom">
                <div className="product-tumb">
                    <img src={data.image} alt={data.title} />
                </div>
            </div>
            <div className="col m6 margin-bottom">
                <h3>{data.title}</h3>
                <div style={{ marginBottom: '32px' }}>
                    <span>Category : <strong>{data.category}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Review : <strong>{data.rate}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Stock : <strong>{data.stock}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Discount : <strong>{data.discount} %</strong></span>
                </div>
                <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                    Rp. {formatNumberToRupiah(data.price)}
                </div>
                <div style={{ marginBottom: '32px' }}>
                {data.description}
                </div>
                <div style={{ marginBottom: '32px' }}>
                    <div><strong>Quantity : </strong></div>
                    <input type="number" value={inputQty} onChange={handleInputQty}   className="input section border" name="total" placeholder='Quantity' />
                </div>
                <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                    Sub Total : Rp. {formatNumberToRupiah(inputQty * data.price)}
                    {data.discount > 0 ? <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>Rp. {formatNumberToRupiah((inputQty * data.price) - ((inputQty * data.price) * (data.discount/100)) )}</strong></span> : ''}
                </div>
                {
                    data.stock > 0 ? <button onClick={addToCart} className='button light-grey block' disabled={inputQty === 0}>Add to cart</button> : <button className='button light-grey block' disabled={true}>Empty Stock</button>
                }
                
            </div>
        </div>
        }
        
    </div>
    )
}

export default Detail