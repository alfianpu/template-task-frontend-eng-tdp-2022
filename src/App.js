
import 'assets/css/custom.css';
import 'assets/css/style.css';
import Layout from 'layout';
import Home from 'page/home';
import Shop from 'page/shop';
import Detail from 'page/shop/detail';
import { Route, Routes } from 'react-router-dom';


const App = () => {
    
    return (
        <Routes>
            <Route path='/' element={<Layout><Home/></Layout>}></Route>
            <Route path='/shop' element={<Layout><Shop/></Layout>}></Route>
            <Route path='/shop/:id' element={<Layout><Detail/></Layout>}></Route>
           
        </Routes>
    );
}

export default App;
