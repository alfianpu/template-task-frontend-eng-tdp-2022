import { Link } from "react-router-dom"

const Header = () => {
    return (
        <div className="top" >
            <div className="bar white wide padding card">
                <Link to="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</Link>
                <div className="right hide-small">
                    <Link
                        to="/"
                        className="bar-item button"
                    >
                        Home
                    </Link>
                    <Link
                        to="/shop"
                        className="bar-item button"
                    >
                        Shop
                    </Link>
                    
                    
                    
                </div>
            </div>
        </div>
    )
}

export default Header