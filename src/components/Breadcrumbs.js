import { Link } from "react-router-dom"
const Breadcrumbs = ({data}) => {
    return (
        <div className='breadcrumb'>
            {
                data.map(({ to, label }, key) => (
                    <span key={key}>
                        {
                            (to) ? <Link to={to}>{label}</Link> : label
                        }
                        {(key !== data.length - 1) ? <span className="breadcrumb-separator">/</span> : null}
                    </span>
                ))
            }
        </div>
    )
}
export default Breadcrumbs
